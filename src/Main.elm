module Main exposing (..)

import Browser
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html exposing (main_)
import Html.Attributes as HtmlAttr
import List.Extra as Lst



-- MAIN


main : Program () Model Msg
main =
    Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
    { root : List Node
    , min : Int
    , input : String
    , selected : List Int
    , table : List (Maybe Int)
    , consolidate : Bool
    , autoConsolidate : Bool
    , autoIncrement : Int
    }


type alias Node =
    { value : Int
    , marked : Bool
    , children : Children
    }


type Children
    = Nodes (List Node)


init : Model
init =
    { root = dummyTree
    , min = 0
    , input = ""
    , selected = [ 1, 2, 1 ]
    , table = [ Just 0, Nothing, Nothing, Nothing, Just 1 ]
    , consolidate = False
    , autoConsolidate = True
    , autoIncrement = 1
    }


dummyTree : List Node
dummyTree =
    [ mkNode 1 False []
    , mkNode 2
        False
        [ mkNode 3 True []
        , mkNode 4 False []
        , mkNode 5 True [ mkNode 9 False [], mkNode 10 False [] ]
        , mkNode 11 False [ mkNode 12 False [], mkNode 13 False [], mkNode 14 False [] ]
        , mkNode 6 False []
        , mkNode 7 False []
        , mkNode 8 False []
        ]
    ]


mkNode val mark children =
    Node val mark (Nodes children)



-- UPDATE


type Msg
    = Input String
    | SetAutoConsolidate Bool
    | SetAutoIncrement Bool
    | SetAutoDecrement Bool
    | Select (List Int)
    | Insert
    | DeleteMin
    | Delete
    | Consolidate


update : Msg -> Model -> Model
update msg model =
    case msg of
        Input val ->
            if (String.toInt val /= Nothing || val == "" || val == "-") && String.length val <= 4 then
                { model | input = val }

            else
                model

        SetAutoConsolidate c ->
            { model | autoConsolidate = c }

        SetAutoIncrement c ->
            if c then
                { model | autoIncrement = 1 }

            else
                { model | autoIncrement = 0 }

        SetAutoDecrement c ->
            if c then
                { model | autoIncrement = -1 }

            else
                { model | autoIncrement = 0 }

        Select path ->
            { model | selected = path }

        Insert ->
            let
                val =
                    model.input
                        |> String.toInt
                        |> Maybe.withDefault 0

                v2 =
                    val + model.autoIncrement
            in
            Node val False (Nodes [])
                |> (\n -> insertNode n { model | input = iToS v2 })

        DeleteMin ->
            deleteMin model

        Delete ->
            if model.selected == [ model.min ] then
                deleteMin model

            else
                model |> deleteNode model.selected

        Consolidate ->
            model |> consolidate |> (\m -> { m | consolidate = canConsolidate m.root })


deleteMin model =
    model
        |> deleteNode [ model.min ]
        |> findMin
        |> (if model.autoConsolidate then
                consolidateAll

            else
                identity
           )
        |> (\m -> { m | consolidate = canConsolidate m.root })


nodeChildren : Node -> List Node
nodeChildren node =
    case node.children of
        Nodes val ->
            val


findMin : Model -> Model
findMin model =
    let
        values =
            model.root |> List.map .value

        min =
            values |> List.minimum |> Maybe.withDefault 0

        minIndex =
            values |> Lst.findIndex ((==) min) |> Maybe.withDefault 0
    in
    { model | min = minIndex }


getNode : List Int -> List Node -> Maybe Node
getNode path nodes =
    case path of
        [] ->
            Nothing

        i :: [] ->
            Lst.getAt i nodes

        i :: tail ->
            case Lst.getAt i nodes of
                Nothing ->
                    Nothing

                Just n ->
                    getNode tail <| nodeChildren n


getMinNode : Model -> Maybe Node
getMinNode model =
    model.root |> Lst.getAt model.min


insertNode : Node -> Model -> Model
insertNode node model =
    let
        min =
            getMinNode model

        isSmaller =
            min
                |> Maybe.map (\m -> node.value < m.value)
                |> Maybe.withDefault True

        ( rootHead, rootTail ) =
            --  Insert left of min
            model.root |> Lst.splitAt model.min

        minIndex =
            if isSmaller then
                model.min

            else
                model.min + 1

        newRoot =
            rootHead ++ { node | marked = False } :: rootTail
    in
    { model | min = minIndex, root = newRoot }


moveChildrenToRoot node model =
    node
        |> nodeChildren
        |> List.foldl insertNode model


deleteNode path model =
    let
        node =
            model.root
                |> getNode path

        ( r2, orphan ) =
            model.root |> deleteSubTree path

        m2 =
            case node of
                Nothing ->
                    model

                Just n ->
                    case path of
                        [ i ] ->
                            if i < model.min then
                                -- update min index
                                { model | root = r2, selected = [], min = model.min - 1 }
                                    |> moveChildrenToRoot n

                            else
                                { model | root = r2, selected = [] } |> moveChildrenToRoot n

                        _ ->
                            { model | root = r2, selected = [] } |> moveChildrenToRoot n
    in
    orphan |> List.foldl insertNode m2


markNodeInPath path nodes =
    case path of
        i :: [] ->
            nodes

        _ ->
            nodes |> updateInPath path (\n -> { n | marked = True })


canConsolidate nodes =
    (nodes |> List.map (\n -> List.length (nodeChildren n)) |> findPair) /= Nothing


nodeValue i nodes =
    nodes
        |> getNode [ i ]
        |> Maybe.map .value
        |> Maybe.withDefault 0


consolidateAll : Model -> Model
consolidateAll model =
    let
        m2 =
            model |> consolidate
    in
    if canConsolidate m2.root then
        consolidateAll m2

    else
        m2


consolidate : Model -> Model
consolidate model =
    let
        pair =
            model.root |> List.map (\n -> List.length (nodeChildren n)) |> findPair
    in
    case pair of
        Nothing ->
            model

        Just ( x, y ) ->
            let
                ( a, b ) =
                    if nodeValue x model.root <= nodeValue y model.root then
                        ( x, y )

                    else
                        ( y - 1, x )

                nodeB : Maybe Node
                nodeB =
                    getNode [ b ] model.root
            in
            case nodeB of
                Nothing ->
                    -- if this happens, something has a bug (e.g. findPair)
                    model

                Just nb ->
                    let
                        ( r2, orphan ) =
                            model.root
                                |> deleteSubTree [ b ]

                        min =
                            if b < model.min then
                                model.min - 1

                            else
                                model.min

                        m2 =
                            orphan
                                |> List.foldl insertNode { model | root = r2 }

                        r3 =
                            m2.root
                                |> updateInPath [ a ]
                                    (\n ->
                                        case n.children of
                                            Nodes cs ->
                                                { n | children = Nodes (nb :: cs) }
                                    )
                    in
                    { model | root = r3, min = min }


findPair : List Int -> Maybe ( Int, Int )
findPair list =
    case list of
        a :: b :: xs ->
            findPairHelper [ a ] b xs

        _ ->
            Nothing


findPairHelper prev i tail =
    case Lst.findIndex ((==) i) prev of
        Nothing ->
            case tail of
                [] ->
                    Nothing

                j :: rest ->
                    findPairHelper (prev ++ [ i ]) j rest

        Just index ->
            Just ( index, List.length prev )


{-| Get a tree where the subtree is removed, and the parent node is marked
If the parent node was marked, it is in the second Tuple value,
If the parent and it's parent were marked
-}
deleteSubTree : List Int -> List Node -> ( List Node, List Node )
deleteSubTree path nodes =
    let
        parentPath =
            path |> dropRight 1

        parent =
            getNode parentPath nodes
    in
    case parent of
        Nothing ->
            ( nodes |> deleteSubTreeNoMark path, [] )

        Just p ->
            if p.marked then
                let
                    rm2 =
                        nodes |> deleteSubTreeNoMark path

                    parent2 =
                        rm2 |> getNode parentPath

                    ( rm3, removedNodes ) =
                        deleteSubTree parentPath rm2
                in
                case parent2 of
                    Just p2 ->
                        ( rm3, p2 :: removedNodes )

                    Nothing ->
                        ( rm3, removedNodes )

            else
                ( nodes
                    |> markNodeInPath parentPath
                    |> deleteSubTreeNoMark path
                , []
                )


updateInPath path transform nodes =
    case path of
        [] ->
            -- nothing selected
            nodes

        i :: [] ->
            --
            Lst.updateAt i transform nodes

        i :: tail ->
            case Lst.getAt i nodes of
                Nothing ->
                    nodes

                Just n ->
                    Lst.updateAt i
                        (\_ ->
                            let
                                newChildren =
                                    n |> nodeChildren |> updateInPath tail transform
                            in
                            Node n.value n.marked (Nodes newChildren)
                        )
                        nodes


deleteSubTreeNoMark path nodes =
    case path of
        [] ->
            -- nothing selected
            nodes

        i :: [] ->
            --
            Lst.removeAt i nodes

        i :: tail ->
            case Lst.getAt i nodes of
                Nothing ->
                    nodes

                Just n ->
                    Lst.updateAt i
                        (\_ ->
                            let
                                newChildren =
                                    n |> nodeChildren |> deleteSubTreeNoMark tail
                            in
                            Node n.value n.marked (Nodes newChildren)
                        )
                        nodes


view model =
    layout [ height fill ] <|
        column [ spacing 30, centerX, paddingXY 20 30, height fill ]
            [ el [ centerX ] <| text "Fibonacci heap"
            , commands model
            , settings model
            , showTree model
            , link [ alignBottom, centerX, Font.size 10, Font.color <| rgb 0 0 0.5 ]
                { url = "https://f0i.de/impressum"
                , label = text "impressum"
                }
            ]


commands : Model -> Element Msg
commands model =
    wrappedRow
        [ spacing 10, centerX ]
        (if model.consolidate then
            [ btn "consolidate" Consolidate ]

         else
            [ Input.text [ width <| px 100 ]
                { onChange = Input
                , text = model.input
                , placeholder = Just <| Input.placeholder [] <| text "0"
                , label = Input.labelRight [] (text "")
                }
            , btn "insert" Insert
            , btn "delete-min" DeleteMin
            ]
        )


settings model =
    column [ spacing 10, centerX ]
        [ Input.checkbox []
            { onChange = SetAutoIncrement
            , icon = Input.defaultCheckbox
            , checked = model.autoIncrement > 0
            , label = Input.labelRight [] <| text "auto increment"
            }
        , Input.checkbox []
            { onChange = SetAutoDecrement
            , icon = Input.defaultCheckbox
            , checked = model.autoIncrement < 0
            , label = Input.labelRight [] <| text "auto decrement"
            }
        , Input.checkbox []
            { onChange = SetAutoConsolidate
            , icon = Input.defaultCheckbox
            , checked = model.autoConsolidate
            , label = Input.labelRight [] <| text "auto consoledate"
            }
        ]


btn label msg =
    Input.button []
        { onPress = Just msg
        , label =
            el
                [ Border.width 1
                , mouseDown [ Background.color <| rgb 0.8 0.8 0.8 ]
                , paddingXY 5 10
                , Border.rounded 4
                ]
            <|
                text label
        }


iToS : Int -> String
iToS =
    String.fromInt


takeRight n list =
    list |> List.reverse |> List.take n |> List.reverse


dropRight n list =
    list |> List.reverse |> List.drop n |> List.reverse


showTree : Model -> Element Msg
showTree model =
    row
        [ spacing 20
        , alignTop
        , paddingXY 0 18
        , behindContent <|
            el
                [ width fill
                , Border.widthEach { top = 0, left = 0, right = 0, bottom = 4 }
                , Border.color <| rgba 0 0 0 0.1
                , paddingXY 0 20
                ]
                none
        ]
        (model.root
            |> List.indexedMap
                (\i n ->
                    el
                        [ if i == model.min then
                            above (el [ alignRight, Font.color <| rgb 0 0.5 0, Font.bold ] <| text "min \n  ↓")

                          else
                            above none
                        , alignTop
                        ]
                        (showSubTree [ i ] model.selected n)
                )
        )


showSubTree : List Int -> List Int -> Node -> Element Msg
showSubTree path selected node =
    let
        val =
            node.value |> iToS |> text

        sel =
            path == selected

        inPath =
            Lst.zip path selected
                |> Lst.find (\( a, b ) -> a /= b)
                |> (/=) Nothing
    in
    column [ spacing 0, alignTop, alignRight ]
        [ Input.button
            [ alignRight
            , Border.color <|
                if path == selected then
                    rgb 0 0.4 0

                else
                    rgb 0 0 0
            , Border.width 4
            , Border.rounded 25
            , Background.color <|
                if node.marked then
                    rgb 0.7 0.3 0.3

                else
                    rgb 1 1 1
            , width <| px 50
            , height <| px 50
            ]
            { onPress =
                if sel then
                    Just <| Select []

                else
                    Just <| Select path
            , label = el [ centerX, centerY ] val
            }
        , if sel then
            el [ alignRight ] <| btn "delete" Delete

          else
            none
        , if (node |> nodeChildren) == [] then
            none

          else
            el [ width fill, height <| px 40, paddingXY 25 0 ] <|
                el [ width fill, height <| px 40, Border.widthEach { left = 0, right = 4, top = 0, bottom = 0 }, Border.color <| rgba 0 0 0 0.1 ] <|
                    html <|
                        Html.img
                            [ --HtmlAttr.src "/tree.png"
                              HtmlAttr.src "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAAAAABVicqIAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAHdElNRQflBBYPLBlcqQpBAAABPUlEQVRo3t3NuZUCQRBEwW8rEv7rsCvwuGem68hKgXAg+Bt2PQPDx+XEeHI7ZpPrmfHkfkwmj2MweR5zycsxlrweU8nbMZS8HzPJxzGSfB4TydcxkHwf+mTjkCdbhzrZPMTJ9qFNdg5psncok91DmOwfuuTgkCVHhyo5PETJ8aFJFockWR2KZHkIkvXRTwJHO4kc3SR0NJPY0UuCRyuJHp0kfDSS+FFPEkc5yRzVJHUUk9xRS5JHKckelSR9FJL8kU8KRzqpHNmkdCST2pFLikcqqR6ZpHwkkvoRTxpHOOkc0aR1BJPeEUuaRyjpHpGkfQSS/rFOBMcyURyrRHLgOHAcOA4cB44Dx4HjwHHgOHAcOA4cB44Dx4HjwHHgOHAcOA4cB44Dx4HjwHHgOHAcOA4cB44Dx4Hj4GQ4fsc/WGam2Y9J7vkAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjEtMDQtMjJUMTU6NDQ6MjUrMDA6MDCITxMzAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIxLTA0LTIyVDE1OjQ0OjI1KzAwOjAw+RKrjwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII="
                            , HtmlAttr.style "object-fit" "fill"
                            , HtmlAttr.style "width" "100%"
                            , HtmlAttr.style "position" "absolute"
                            , HtmlAttr.style "height" "100%"
                            , HtmlAttr.style "opacity" "0.1"
                            ]
                            []

        --image [ width fill, height <| px 40, paddingXY 20 0, htmlAttribute <| HtmlAttr.style "object-fit" "fill" ] { src = "/tree.png", description = "" }
        , row [ spacing 30 ]
            (node
                |> nodeChildren
                |> List.indexedMap (\i n -> showSubTree (path ++ [ i ]) selected n)
            )
        ]
